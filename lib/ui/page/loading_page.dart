import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:insolentiae/bloc/post_list_bloc.dart';
import 'package:insolentiae/bloc/post_list_state.dart';
import 'package:insolentiae/ui/page/list_page.dart';
import 'package:insolentiae/ui/styles.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<PostListBloc, PostListState>(
      listener: (context, state) {
        if (state is PostList) {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => ListPage()),
          );
        }
      },
      child: Scaffold(
        body: Container(
          color: AppStyles.colorPrimary,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "INSOLENTIAE",
                style: GoogleFonts.headlandOne(
                  fontSize: 40.0,
                ),
              ),
              SizedBox(height: 160.0),
              SpinKitFadingCube(
                color: AppStyles.colorOnPrimary,
                size: 40.0,
                duration: const Duration(milliseconds: 2400),
              ),
              SizedBox(height: 160.0),
              BlocBuilder<PostListBloc, PostListState>(
                builder: (context, state) {
                  if (state is Loading) {
                    return Text(
                      state.message,
                      style: TextStyle(
                        fontSize: 16.0,
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
