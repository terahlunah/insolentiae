import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:insolentiae/bloc/post_list_bloc.dart';
import 'package:insolentiae/bloc/post_list_event.dart';
import 'package:insolentiae/bloc/post_list_state.dart';
import 'package:insolentiae/model/post.dart';
import 'package:insolentiae/ui/page/about_page.dart';
import 'package:insolentiae/ui/page/detail_page.dart';
import 'package:insolentiae/ui/styles.dart';
import 'package:insolentiae/ui/widget/slide_left_route.dart';
import 'package:iris/iris.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      backgroundColor: AppStyles.colorPrimary,
      body: buildBody(context),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: AppStyles.colorPrimary,
      title: InkWell(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => AboutPage()),
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                  decoration: BoxDecoration(border: Border(top: BorderSide(width: 2.0, color: AppStyles.colorAccent)))),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text("INSOLENTIAE", style: GoogleFonts.headlandOne()),
            ),
            Expanded(
              child: Container(
                  decoration: BoxDecoration(border: Border(top: BorderSide(width: 2.0, color: AppStyles.colorAccent)))),
            ),
          ],
        ),
      ),
      centerTitle: true,
    );
  }

  Widget buildBody(BuildContext context) {
    return BlocBuilder<PostListBloc, PostListState>(
      builder: (context, state) {
        if (state is PostList) {
          if (state.loading) {
            return ListView.builder(
              itemCount: state.posts.length + 1,
              itemBuilder: (context, index) {
                return (index == state.posts.length)
                    ? buildLoader(context)
                    : buildPostCard(context, state.posts[index], index);
              },
            );
          } else {
            return ListView.builder(
              itemCount: state.posts.length,
              itemBuilder: (context, index) => buildPostCard(context, state.posts[index], index),
            );
          }
        } else {
          return Container();
        }
      },
    );
  }

  Widget buildLoader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 24.0),
      child: SpinKitFadingCube(
        color: AppStyles.colorOnPrimary,
        size: 30.0,
        duration: const Duration(milliseconds: 2400),
      ),
    );
  }

  Widget buildPostCard(BuildContext context, Post post, int index) {
    BlocProvider.of<PostListBloc>(context).add(PostListEvent.scrolled(index));

    return InkWell(
      onTap: () => Navigator.push(
        context,
        SlideLeftRoute(page: DetailPage(post)),
      ),
      child: Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          color: AppStyles.colorSurface,
          child: ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
              title: Text(
                post.title.minLines(3),
                style: TextStyle(color: AppStyles.colorOnSurface),
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
              ),
              // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

              subtitle: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: <Widget>[
                        Icon(Icons.subject, color: AppStyles.colorAccent),
                        SizedBox(width: 5.0),
                        Text(
                          post.category,
                          style: TextStyle(color: AppStyles.colorAccent),
                          overflow: TextOverflow.ellipsis,
                        )
                      ],
                    ),
                    Text(
                      post.publishedDate,
                      style: TextStyle(color: AppStyles.colorAccent),
                    ),
                  ],
                ),
              ),
              trailing: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Icon(Icons.keyboard_arrow_right, color: AppStyles.colorOnSurface, size: 30.0),
              )),
        ),
      ),
    );
  }
}
