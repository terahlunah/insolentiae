import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:insolentiae/ui/styles.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyles.colorPrimary,
      appBar: buildAppBar(context),
      body: buildBody(context),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: AppStyles.colorPrimary,
      title: Text("A propos"),
    );
  }

  Widget buildBody(BuildContext context) {
    return Container(
      color: AppStyles.colorPrimary,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 32.0),
              child: Container(
                decoration: BoxDecoration(border: Border(top: BorderSide(width: 2.0, color: AppStyles.colorAccent))),
                child: SizedBox(width: 100.0),
              ),
            ),
            RichText(
              text: TextSpan(
                text: "Cette application ",
                style: TextStyle(fontSize: 16.0),
                children: [
                  TextSpan(
                    text: "NON OFFICIELLE",
                    style: TextStyle(color: AppStyles.colorAccent),
                  ),
                  TextSpan(
                    text:
                        " vise à fournir une navigation native et une lecture épurée des articles du site Insolentiae.com",
                  ),
                ],
              ),
            ),
            Text(
              "Créée originalement pour mon confort personnel, j'ai décidé de la mettre à disposition de tous gratuitement.",
              style: TextStyle(fontSize: 16.0),
            ),
            Text(
              "Je ne suis pas l'auteur des articles listés ici, ceux-cis sont issus du site Insolentiae.com et écris par Charles Sannat.",
              style: TextStyle(fontSize: 16.0),
            ),
            Text(
              "Bonne lecture !",
              style: TextStyle(fontSize: 16.0),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 32.0),
              child: Container(
                decoration: BoxDecoration(border: Border(top: BorderSide(width: 2.0, color: AppStyles.colorAccent))),
                child: SizedBox(width: 100.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
