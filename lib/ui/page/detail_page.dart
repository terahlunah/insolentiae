import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:insolentiae/model/post.dart';
import 'package:insolentiae/ui/styles.dart';

class DetailPage extends StatelessWidget {
  final Post post;

  DetailPage(this.post);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyles.colorPrimary,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) => [buildAppBar(context)],
        body: buildBody(context),
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return SliverAppBar(
      expandedHeight: 200.0,
      floating: true,
      pinned: false,
      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.parallax,
        background: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                post.imageUrl,
              ),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(height: 16.0),
            buildTitle(context),
            SizedBox(height: 32.0),
            HtmlWidget(
              post.content,
              bodyPadding: EdgeInsets.all(0.0),
              config: HtmlWidgetConfig(textStyle: TextStyle(fontSize: 16.0)),
            ),
            SizedBox(height: 32.0),
            buildPresslib(context),
            SizedBox(height: 32.0),
          ],
        ),
      ),
    );
  }

  Widget buildTitle(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(left: BorderSide(width: 2.0, color: AppStyles.colorAccent))),
      padding: EdgeInsets.only(left: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            post.title,
            softWrap: true,
            style: TextStyle(fontSize: 20.0),
          ),
          SizedBox(height: 8.0),
          Text(
            post.publishedDate,
            style: TextStyle(
              fontSize: 15.0,
              color: AppStyles.colorAccent,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPresslib(BuildContext context) {
    return Container(
      decoration: BoxDecoration(border: Border(top: BorderSide(width: 2.0, color: AppStyles.colorAccent))),
      padding: EdgeInsets.only(top: 16.0),
      child: Text(
          "« Ceci est un article « presslib » et sans droit voisin, c’est-à-dire libre de reproduction en tout ou en partie à condition que le présent alinéa soit reproduit à sa suite. Insolentiae.com est le site sur lequel Charles Sannat s’exprime quotidiennement et livre un décryptage impertinent et sans concession de l’actualité économique. Merci de visiter mon site. Vous pouvez vous abonner gratuitement à la lettre d’information quotidienne sur www.insolentiae.com. »"),
    );
  }
}
