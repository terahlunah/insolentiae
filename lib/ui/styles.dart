import 'package:flutter/material.dart';

abstract class AppStyles {
  static const Color colorPrimary = Color.fromRGBO(58, 66, 86, 1.0);
  static const Color colorOnPrimary = Colors.white;
  static const Color colorSurface = Color.fromRGBO(64, 75, 96, 1.0);
  static const Color colorOnSurface = Colors.white;
  static const Color colorAccent = Color.fromRGBO(232, 197, 71, 1.0);
}
