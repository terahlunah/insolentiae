import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:insolentiae/bloc/post_list_bloc.dart';
import 'package:insolentiae/ui/page/loading_page.dart';
import 'package:insolentiae/ui/styles.dart';

class InsolentiaeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme.apply(
          bodyColor: AppStyles.colorOnPrimary,
          displayColor: AppStyles.colorOnPrimary,
        );

    return BlocProvider<PostListBloc>(
      builder: (context) => PostListBloc(),
      child: MaterialApp(
          title: 'Insolentiae',
          theme: ThemeData(
            primaryColor: AppStyles.colorPrimary,
            accentColor: AppStyles.colorOnSurface,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            textTheme: textTheme,
          ),
          home: LoadingPage(),
          debugShowCheckedModeBanner: false),
    );
  }
}
