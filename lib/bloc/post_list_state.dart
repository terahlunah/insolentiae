import 'package:insolentiae/model/post.dart';

abstract class PostListState {
  factory PostListState.loading(String message) = Loading;

  factory PostListState.postList(List<Post> posts, bool loading) = PostList;

  factory PostListState.error() = Error;
}

class Loading implements PostListState {
  String message;

  Loading(this.message);
}

class PostList implements PostListState {
  List<Post> posts;
  bool loading;

  PostList(this.posts, this.loading);
}

class Error implements PostListState {}
