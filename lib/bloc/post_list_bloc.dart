import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:insolentiae/model/index.dart';
import 'package:insolentiae/model/post.dart';
import 'package:insolentiae/service/insolentiae_service.dart';

import 'post_list_event.dart';
import 'post_list_state.dart';

class PostListBloc extends Bloc<PostListEvent, PostListState> {
  final _service = InsolentiaeService();

  Index index;
  List<Post> posts = [];
  int maxIndex = 0;

  PostListBloc() {
    add(PostListEvent.load());
  }

  @override
  PostListState get initialState => PostListState.loading("Chargement de l'index");

  @override
  Stream<PostListState> mapEventToState(PostListEvent event) async* {
    try {
      if (event is Load) {
        yield* load(0);
      }
      if (event is ListScrolled) {
        yield* loadMoreIfNeeded(event.index);
      }
    } catch (ex) {
      print(ex.toString());
      yield PostListState.postList([], false);
    }
  }

  Stream<PostListState> loadMoreIfNeeded(int index) async* {
    maxIndex = max(index, maxIndex);

    if (state is PostList && !(state as PostList).loading) {
      if (posts.length - maxIndex < 20) {
        yield PostListState.postList(posts, true);
        yield* load(posts.length);
      }
    }
  }

  Stream<PostListState> load(int start) async* {
    index = index ?? await _service.getPostIndex();

    posts += await Future.wait(index.posts.skip(start).take(10).map((it) => _service.getPost(it)));

    yield PostListState.postList(posts, false);

    yield* loadMoreIfNeeded(0);
  }
}
