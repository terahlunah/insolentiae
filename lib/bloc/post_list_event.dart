abstract class PostListEvent {
  factory PostListEvent.load() = Load;
  factory PostListEvent.scrolled(int index) = ListScrolled;
}

class Load implements PostListEvent {}

class ListScrolled implements PostListEvent {
  int index;

  ListScrolled(this.index);
}
