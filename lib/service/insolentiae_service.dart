import 'package:flutter/foundation.dart';
import 'package:html/dom.dart';
import 'package:html/parser.dart' as html;
import 'package:http/http.dart' as http;
import 'package:insolentiae/model/index.dart';
import 'package:insolentiae/model/post.dart';
import 'package:insolentiae/model/resource.dart';
import 'package:iris/iris.dart';
import 'package:xml/xml.dart' as xml;

const INSOLENTIAE_SITEMAP_URL = 'https://insolentiae.com/sitemap_index.xml';

class InsolentiaeService with CacheProvider {
  Future<Post> getPost(Resource postResource) async {
    final response = await cache(postResource.url, http.get(postResource.url));
    return await compute(parsePost, response.body);
  }

  Future<List<Resource>> getPostSitemaps() async {
    final response = await cache(INSOLENTIAE_SITEMAP_URL, http.get(INSOLENTIAE_SITEMAP_URL));
    final sitemap = await compute(parseSitemap, response.body);
    final sitemaps = sitemap.findAllElements("sitemap");
    final sitemapsResources = sitemaps.map((it) => Resource.fromXML(it));
    final postsSitemaps = sitemapsResources.where((it) => it.url.contains("post-sitemap"));

    return postsSitemaps.toList();
  }

  Future<List<Resource>> getPostsResourcesFromSitemap(Resource sitemap) async {
    final response = await cache(sitemap.url, http.get(sitemap.url));
    final document = await compute(parseSitemap, response.body);
    final posts = document.findAllElements("url").map((it) => Resource.fromXML(it));

    return posts.toList();
  }

  Future<Index> getPostIndex() async {
    final postsSitemaps = await getPostSitemaps();
    final postsResources = await Future.wait(postsSitemaps.map((s) => getPostsResourcesFromSitemap(s)));
    final cleaned = postsResources.expand((it) => it).toList().reversed.toList();

    return Index(cleaned);
  }
}

xml.XmlDocument parseSitemap(String body) {
  return xml.parse(body);
}

Post parsePost(String body) {
  final document = html.parse(body);
  final title = document.getElementsByClassName("entry-title").first.text;
  final content = document.getElementsByClassName("b-article__header-title").firstOrNull ??
      document.getElementsByClassName("entry-content").last;
  final publishedDate = document.getElementsByClassName("published").first.text;
  final category = document
      .getElementsByTagName("a")
      .where((it) => it.attributes.containsKey("rel"))
      .where((it) => it.attributes["rel"] == "category tag")
      .where((it) => !it.attributes["href"].contains("article-entete"))
      .first
      .text;
  final imageUrl = document
      .getElementsByClassName("et_post_meta_wrapper")
      .expand((it) => it.getElementsByTagName("img"))
      .where((it) => it != null)
      .first
      .attributes["src"];

  return Post(title, cleanContent(content), imageUrl, publishedDate, category);
}

String cleanContent(Node node) {
  final elements = node.children
      .where((element) => (element.localName != "div"))
      .where((element) => !element.innerHtml.contains("lettre mensuelle"))
      .where((element) => !element.innerHtml.contains("« Insolentiae » signifie « impertinence » en latin"))
      .where((element) => !element.innerHtml.contains("https://insolentiae.com/wp-content/uploads/"))
      .where((element) => !element.innerHtml.contains("presslib"));
  return elements.map((e) => e.outerHtml).join();
}
