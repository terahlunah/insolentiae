import 'package:iris/iris.dart';
import 'package:xml/xml.dart';

class Resource {
  String url;
  DateTime date;
  String image;

  Resource(this.url, this.date, this.image);

  Resource.fromXML(XmlElement element) {
    url = element.findElements("loc").firstOrNull?.text;
    var dateRaw = element.findElements("lastmod").firstOrNull?.text;
    date = dateRaw?.let((it) => DateTime.parse(it));
    image = element.findAllElements("image:loc").firstOrNull?.text;
  }
}
