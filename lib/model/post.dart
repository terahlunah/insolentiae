class Post {
  String title;
  String content;
  String imageUrl;
  String publishedDate;
  String category;

  Post(this.title, this.content, this.imageUrl, this.publishedDate, this.category);
}
